#!/usr/bin/python3

from stats import insertion_sort

def mode(values):
    """
    pre: `values` est un tableau (list) d objets comparables
    post: renvoie le mode du tableau
    """
    a = insertion_sort(values)
    best, best_length, length = a[0], 0, 0

    for i in range(1, len(a)):
        if a[i-1] == a[i]:
            length += 1
        elif length > best_length:
            best= a[i-1]
            best_length = length
            length = -1

    if length > best_length:
        best = a[i]

    return best



def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)



############
#Exemple de test:
if not mode([1,2,2,3,3,3,4,4,4,4]) == 4:
    error("Erreur de mode")
