#!/usr/bin/python3

class search_result:
    #Some code...
    def __init__(self, url, snippet, score):
        """
        pre: -
        post: initialize a new seach result
        """
        self.url = url
        self.snippet = snippet
        self.score = score
        pass #Some more code...

    def compare_to(self, o):
        """
        Comparaison de `search_result` sur base des scores
        pre: `o` est un `search_result`
        post: renvoie -1 si `self` est plut petit que `o`
                       0    les deux objets sont equivalents
                      +1 si `self` est plus grand que `o`
        """
        x = self.score - o.score
        return (x > 0) - (x < 0)

    def __str__(self):
        return "["+str(self.url)+","+str(self.snippet)+","+str(self.score)+"]"
