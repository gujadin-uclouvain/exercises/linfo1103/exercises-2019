#!/usr/bin/python3

from search_result import search_result

class searcher:
    def search(self, query): #Do some job
        pass

    def sort_search_results(self, results):
        """
        pre: `results` est un tableau (list) de `search_result`
        post: tri par sélection du tableau passé en paramètre.
            Les élements sont triés en place selon l ordre défini
            dans la classe `search_result`
        """
        for i in range(0, len(results)):
            min = i
            for j in range(i, len(results)):
                if results[j].compare_to(results[min]) == 1:
                    min = j
            if min != i:
                results[i], results[min] = results[min], results[i]


def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)


############
#Exemple de test:

############
###Une fonction qui crée des search_result est définie
###Elle servira à tester votre code, puisque la méthode
###search de la classe searcher ne vous est pas donnée.
############
def build_search_results(i):
    return search_result(url = 'url.' + str(i), snippet = 'snippet.' + str(i), score = i)
############


#Crée un nouveau `searcher`
my_searcher = searcher()

#Crée une liste de search_result
seach_results_list = []

#Les search_results y sont ajoutés
for i in range(1, 10):
    sr = build_search_results(i)
    seach_results_list.append(sr)

#Les search_results originaux sont affichés, un par ligne
print('\n'.join([str(sr) for sr in seach_results_list]))

print('\n--\n')

#Trie en place de la liste
my_searcher.sort_search_results(seach_results_list)

#Parcours la liste, vérifie que l'ordre est bon en affichant le résultat
for i in range(len(seach_results_list)):
    print(seach_results_list[i])
    
    #Plutôt que de vérifier à la vue si les résultats sont dans le bon ordre,
    #essayez d'imaginer un test que vous pourriez réaliser étant donné les
    #méthodes à disposition.