#!/usr/bin/python3

from stats import insertion_sort

def median(values):#prendre le milieu d'un tableau
    """
    pre: `values` est un tableau (list) d objets comparables
    post: renvoie la mesure médiane du tableau `values`
    """
    new_values = insertion_sort(values)
    if len(new_values) % 2 == 0:
        return (new_values[len(new_values) // 2] + new_values[(len(new_values) // 2) -1]) /2
    else:
        return new_values[(len(new_values) - 1) // 2]



def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)



############
#Exemple de test:
if not median([2019, 2, 0, 1, 9]) == 2:
    error("Erreur de médiane")
