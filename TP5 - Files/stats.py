#!/usr/bin/python3

def insertion_sort(values):
    """
    pre: `values` est un tableau (list) d objets comparables
    post: renvoie un tableau trié par ordre croissant
    """
    new_values = values.copy()
    for i in range(len(new_values)):
        key = new_values[i]
        j = i-1

        while j >= 0 and new_values[j] > key:
            new_values[j+1] = new_values[j]
            j = j-1
        new_values[j+1] = key

    return new_values



def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)



############
#Exemple de test:
if not insertion_sort([2019, 2, 0, 1, 9]) == [0,1,2,9,2019]:
    error("Tri par insertion ne fonctionne pas")
