#! /usr/bin/python3

from List import List

def maximum(l):
    """
    Calcule le maximum d une liste à l aide d un boucle
    pre: `l` contient au moins un élément
    post: retourne l élément maximum de `l`
    """
    counter = l.head()
    while not l.tail().is_empty():
        if l.tail().head() > counter:
            counter = l.tail().head()
        l =  l.tail()
    return counter



def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)

#Exemples tests:
l = List()
l = l.concat(5)
l = l.concat(4)
l = l.concat(3)
l = l.concat(2)
l = l.concat(1)

if not maximum(l) == 5:
    error("Erreur : le maximum de la liste " + str(l) + " est 5.")
