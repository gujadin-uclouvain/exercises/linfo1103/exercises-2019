#! /usr/bin/python3

from List import List

def length(l):
    """
    Calcule la taille d une liste à l aide d une boucle
    pre: -
    post: retourne la taille de la liste `l`
    """
    counter = 0
    while not l.is_empty():
        counter += 1
        l = l.tail()
    return counter


def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)

#Exemples tests:
l = List()
l = l.concat(5)
l = l.concat(4)
l = l.concat(3)
l = l.concat(2)
l = l.concat(1)

if not length(l) == 5:
    error("Erreur : liste " + str(l) + " est de longueur 5.")
