#! /usr/bin/python3

from List import List

def cum_sum(l):
    """
    Calcule la liste des sommes cumulées des éléments de `l`
    par itération
    pre: -
    post: retourne une liste dont le `i`^ième élément est la somme
        des `i` premiers éléments de `l`
    """
    somme = 0
    inverse_l = List()
    good_l = List()

    while not l.is_empty():
        somme += l.head()
        inverse_l = inverse_l.concat(somme)
        l = l.tail()

    while not inverse_l.is_empty():
        good_l = good_l.concat(inverse_l.head())
        inverse_l = inverse_l.tail()

    return good_l


def cum_sum_inv(l):
    somme = 0
    inverse_l = List()

    while not l.is_empty():
        somme += l.head()
        inverse_l = inverse_l.concat(somme)
        l = l.tail()

    return inverse_l


def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)


#Exemples tests:
l = List()
l = l.concat(5)
l = l.concat(4)
l = l.concat(3)
l = l.concat(2)
l = l.concat(1)
cm = cum_sum(l)
if not cm.head() == 1:
    error("Erreur avec cum_sum")
if not cm.tail().head() == 3:
    error("Erreur avec cum_sum")
