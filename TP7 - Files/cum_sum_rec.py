#! /usr/bin/python3

from List import List

def cum_sum_rec(l):
    """
    Calcule la liste des sommes cumulées des éléments de `l`
    par récursion
    pre: -
    post: retourne une liste dont le `i`^ième élément est la somme
        des `i` premiers éléments de `l`
    """
    def cum_sum_rec2(l, somme=0):
        if l.is_empty():
            return List()

        else:
            somme += l.head()
            tmp = cum_sum_rec2(l.tail(), somme)
            tmp = tmp.concat(somme)
            return tmp

    return cum_sum_rec2(l)


def cum_sum_rec_inv(l, inv=0):
    l = cum_sum_rec(l)
    if l.is_empty():
        return inv

    else:
        inv = inv.concat(l.head())
        return cum_sum_rec_inv(l.tail(), inv)

def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)

#Exemples tests:
l = List()
l = l.concat(5)
l = l.concat(4)
l = l.concat(3)
l = l.concat(2)
l = l.concat(1)
cm = cum_sum_rec(l)
if not cm.head() == 1:
    error("Erreur avec cum_sum")
if not cm.tail().head() == 3:
    error("Erreur avec cum_sum")
