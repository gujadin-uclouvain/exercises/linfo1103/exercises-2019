#!/usr/bin/python3

from file import File

def bin_to_dec(n):
    """
    pre: `n` est une file renvoyant la représentation binaire
        d un nombre en commençant par le bit de poids fort
    post: renvoie la valeur de `n` dans sa forme décimale
    """
    dec = 0
    while len(n) > 0:
        dec += n.dequeue() *2 ** len(n)
    return dec


def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)

#Exemples de tests:
file = File()
for i in [1, 0, 1, 0, 1, 0]:
    file.enqueue(i)

if not bin_to_dec(file) == 42:
    error("La représentation décimale de [1>0>1>0>1>0] "\
            "(avec en indice 0 le bit de poids fort) devrait être 42.")