class File():
    def __init__(self):
        """
        pre: -
        post: intitialise un nouveau tableau vide.
        """
        self.items = []
        self.start = 0
        self.end = 0
        self.size = 0

    def __str__(self):
        """
        pre: -
        post: renvoi le tableau.
        """
        return self.items

    def __len__(self):
        """
        pre: -
        post: renvoi le nombre d'éléments dans un tableau.
        """
        return self.size

    def tab_size(self):
        l = self.items
        return len(l)

    def enqueue(self, item):
        """
        pre: -
        post: ajoute un nouvel élément à la fin du tableau.
        """
        if self.is_full():
            if self.start > self.end:
                self.items.append(item)
                self.size += 1
                for i in range(len(self.items) -1, self.end, -1):
                    self.items[i] = self.items[i-1]
                self.start += 1

            elif self.start <= self.end:
                self.size += 1

                if self.end == self.tab_size():
                    self.items.append(item)
                else:
                    self.items.insert(self.end, item)
                    self.start += 1

                self.end += 1

        else:
            self.items[self.end] = item
            self.end = self.end + 1
            self.size += 1

    def dequeue(self):
        """
        pre: le tableau n'est pas vide.
        post: retire et renvoi l'élément au début du tableau.
        """
        if self.is_empty():
            raise RuntimeError('Impossible de supprimer des éléments dans un tableau vide')

        else:
            try:
                rt = self.items[self.start]
            except:
                self.start = 0
                rt = self.items[self.start]
            if self.start == self.size:
                self.start = 0
            else:
                self.start += 1
            self.size -= 1
            return rt

    def is_empty(self):
        """
        pre: -
        post: renvoi un booléen True si le tableau est vide, False dans le cas contraire.
        """
        if self.items == [] or self.size == 0:
            return True
        else:
            return False

    def is_full(self):
        """
        pre: -
        post: renvoi un booléen True si le tableau est plein, False dans le cas contraire.
        """
        if self.end == self.tab_size() and self.start != 0 and self.size < self.tab_size():
            self.end = 0

        if self.start == self.end or self.end == self.tab_size():
            return True
        else:
            return False


def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)

#Exemples de tests:
"""
a = File()

a.enqueue("6")
a.enqueue("Salut")
a.enqueue("Guillaume")
a.enqueue("9")
a.enqueue("genre quoi?")

a.dequeue()
a.dequeue()
a.dequeue()
a.dequeue()
#a.dequeue()

a.enqueue("new1")
a.enqueue("new2")
a.enqueue("new3")
a.enqueue("new4")
a.enqueue("new5")

a.enqueue("best1")
a.enqueue("best2")
a.enqueue("best3")

a.dequeue()
a.dequeue()
a.dequeue()

a.enqueue("circle1")
a.enqueue("circle2")
a.enqueue("circle3")
a.enqueue("circle4")
a.enqueue("circle5")
"""
"""
file = File()

item = "un élément"

if not file.is_empty():
    error("Une nouvelle file devrait être vide après initialisation.")

file.enqueue(item)
if not len(file) == 1:
    error("Une file contenant un élément devrait être de taille 1.")

if not file.dequeue() == item:
    error("La méthode dequeue() devrait renvoyer le premier élément ajouté.")
"""