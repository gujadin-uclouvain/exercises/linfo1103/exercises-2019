#! /usr/bin/python3

from file import File

def dec_to_bin(n):
    """
    pre: `n` > 0
    post: renvoie la représentation binaire de `n`.
        Le premier élément de la file renvoyée est le bit de poids fort.
        Le nombre de bits renvoyé est le plus petit possible.
    """
    q = File()
    calc = n
    for bit in range(31, -1, -1):
        if 2**bit > calc and q.is_empty():
            pass
        elif 2**bit <= calc:
            q.enqueue(1)
            calc -= 2**bit
        else:
            q.enqueue(0)
    return q



def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)

#Exemples de tests:
file = dec_to_bin(42)

if not len(file) == 6:
    error("La représentation binaire de 42 comprend 6 bits.")
for bit in [1, 0, 1, 0, 1, 0]:
    if not file.dequeue() == bit:
        error("La représentation binaire de 42 est incorrecte. Attention aux spécifications.")