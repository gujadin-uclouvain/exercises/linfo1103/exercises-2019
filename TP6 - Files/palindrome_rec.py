#! /usr/bin/python3

def is_palindrome_rec(s):
    """
    pre: `s` est un tableau (list)
    post: détermine récursivement si `s` est un palindrome ou non
    """
    if len(s) <= 1:
        return True

    elif s[0] == s[-1]:
        return is_palindrome_rec(s[1:-1])

    else:
        return False





def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)

#Exemples tests:
if not is_palindrome_rec("kayak"):
    error("Erreur : kayak est un palindrome")
