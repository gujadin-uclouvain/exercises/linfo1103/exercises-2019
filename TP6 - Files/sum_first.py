#! /usr/bin/python3

def sum_first(t, n):
    """
    Calcule la somme des `n` premiers éléments de `t` avec une boucle
    pre: `t` un tableau (list) non vide
    pre: `n` un entier tel que 1<=`n`<=len(`t`)
    post: retourne la somme des `n` premiers éléments de `t`
    """
    result = 0
    for g in range(1, n+1):
        result += t[g-1]

    return result



def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)

#Exemples tests:
if not sum_first([1,2,3,5,7,9,11], 2) == 3:
    error("Erreur somme des deux premiers nombres")
