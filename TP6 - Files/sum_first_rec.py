#! /usr/bin/python3

def sum_first_rec(t, n):
    """
    Calcule la somme des `n` premiers éléments de `t` par récursion
    pre: `t` un tableau (list) non vide
    pre: `n` un entier tel que 1<=`n`<=len(`t`)
    post: retourne la somme des `n` premiers éléments de `t`
    """
    if n == 1:
        return t[0]

    else:
        return t[0] + sum_first_rec(t[1:], n-1)


print(sum_first_rec([1,2,3,5,7,9,11], 2))

def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)

#Exemples tests:
if not sum_first_rec([1,2,3,5,7,9,11], 2) == 3:
    error("Erreur somme des deux premiers nombres")
