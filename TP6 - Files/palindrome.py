#! /usr/bin/python3

def is_palindrome(s):
    """
    pre: `s` est un tableau (list)
    post: détermine itérativement si `s` est un palindrome ou non
    """
    if len(s) == 0 or len(s) == 1:
        return True

    for i in range(len(s) // 2):
        if s[i] != s[-1 -i]:
            return False
    return True




def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)

#Exemples tests:
if not is_palindrome("kayak"):
    error("Erreur : kayak est un palindrome")