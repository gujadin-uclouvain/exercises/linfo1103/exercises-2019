#! /usr/bin/python3

def puiss_n(x, n):
    """
    Exponentiation rapide avec boucle
    pre: `n` >= 0
    post: retourne `x`^`n`
    """
    result = 1
    for g in range(1, n+1):
        result *= x

    return result

def puiss_log(x, n):
    """
    Exponentiation rapide avec boucle
    pre: `n` >= 0
    post: retourne `x`^`n`
    """
    result = 1
    while n > 0:
        if n % 2 == 1:
            result *= x
        n //= 2
        x *= x
    return result


def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)

#Exemples tests:
if not puiss_log(10, 0) == 1:
    error("Erreur 10 puissance 0")
