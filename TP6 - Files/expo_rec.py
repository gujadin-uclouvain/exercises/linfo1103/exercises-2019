#! /usr/bin/python3

def puiss_rec(x, n):
    """
    Exponentiation rapide par récursion
    pre: `n` >= 0
    post: retourne `x`^`n`
    """
    if n == 0:
        return 1

    elif n % 2 == 0: # pair
        return puiss_rec(x*x, n/2)

    elif n % 2 == 1: # impair
        return x * puiss_rec(x*x, (n-1)/2)

def puiss_rec_other(x, n):
    """
    Exponentiation rapide par récursion
    pre: `n` >= 0
    post: retourne `x`^`n`
    """
    if n == 0:
        return 1

    else:
        return x * puiss_rec(x, n-1)




def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)

#Exemples tests:
if not puiss_rec(10, 0) == 1:
    error("Erreur 10 puissance 0")
