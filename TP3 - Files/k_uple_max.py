#! /usr/bin/python3

def k_uple_max(a, k):
    """
    Calcule la plus grande somme qui peut être obtenue avec
    exactement `k` éléments du tableau `a`.
    pre: `a` un tableau d'entiers; len(`a`) >= `k`
    post: retourne la plus grande somme identifiée
    """
    nbr_counter = [-999999] * k

    if len(a) > k:
        for g in range(len(a)):
            run = True
            for h in range(len(nbr_counter)):
                if run:
                    if a[g] > nbr_counter[h]:
                        nbr_counter[h] = a[g]
                        nbr_counter.sort()
                        run = False

        sol = 0
        for g in range(len(nbr_counter)):
            sol += nbr_counter[g]
        return sol

    elif len(a) == k:
        sol = 0
        for g in range(len(a)):
            sol += a[g]
        return sol

    else:
        return 1


def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)


# Exemples de test:
if not k_uple_max([0,1,0,1,0], 1) == 1:
    error("La plus grande somme de k=1 éléments de a=[0,1,0,1,0] devrait être 1.")
if not k_uple_max([0,1,2,3,4], 3) == 9:
    error("La plus grande somme de k=3 éléments de a=[0,1,2,3,4] devrait être 9.")
