#! /usr/bin/python3

def k_uple_max(a, k):
    """
    Calcule la plus grande somme qui peut être obtenue avec
    exactement `k` éléments du tableau `a`.
    pre: `a` un tableau d'entiers; len(`a`) >= `k`
    post: retourne la plus grande somme identifiée
    """
    counter = [-999999] * k
    result = 0

    if k == 1:
        for elem in a:
            if elem > counter[0]:
                counter[0] = elem
        return counter[0]

    elif k < len(a):
        for i_counter in range(len(counter)):
            run = True
            for elem in a:
                if run:
                    if elem > counter[i_counter]:
                        counter[i_counter] = elem
                        counter.sort()
                        run = False

        for elem in counter:
            result += elem
        return result

    elif k == len(a):
        for elem in a:
            result += elem
        return result

    else:
        return 1


def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)


# Exemples de test:
if not k_uple_max([1,3,4], 3) == 8:
    error("La plus grande somme de k=3 éléments de a=[1,3,4] devrait être 8.")
if not k_uple_max([0,1,0,1,0], 1) == 1:
    error("La plus grande somme de k=1 éléments de a=[0,1,0,1,0] devrait être 1.")
if not k_uple_max([0,1,2,3,4], 3) == 9:
    error("La plus grande somme de k=3 éléments de a=[0,1,2,3,4] devrait être 9.")
