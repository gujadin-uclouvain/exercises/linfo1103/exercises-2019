#! /usr/bin/python3

def paire_max(a):
    """
    Calcule la somme maximale d'une paire d'entiers de `a`
    pre: `a` est un tableau (list) d'entiers
    pre: len(`a`) >= 2
    post: retourne la somme maximale obtenue à partir d'une
        paire d'entiers de `a`
    """
    max_sum = -999999
    if len(a) >= 2:
        for g in range(len(a)):
            for h in range(g+1, len(a)):
                current_sum = a[g] + a[h]
                if current_sum > max_sum:
                    max_sum = current_sum
                    
        return max_sum
    else:
        return 1



def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)

#Exemples de test:
if not paire_max([0,1]) == 1:
    error("Tableau de deux entiers")
else:
    print(paire_max([0,1]))
if not paire_max([0,1,2,3,4]) == 7:
    error("Paire maximale en fin de tableau")
else:
    print(paire_max([0,1,2,3,4]))
if not paire_max([0,4,2,3,1]) == 7:
    error("Paire maximale mélangée")
else:
    print(paire_max([0,4,2,3,1]))
if not paire_max([4,3,2,1,0,-1,-10]) == 7:
    error("Valeurs négatives")
else:
    print(paire_max([4,3,2,1,0,-1,-10]))
if not paire_max([-2,-1]) == -3:
    error("Valeurs exclusivement négatives")
else:
    print(paire_max([-2,-1]))
    