import time
import random
import matplotlib.pyplot as plt
import numpy as np
from paire_max_efficace import paire_max_efficace
from paire_max import paire_max

def generate_instance(size, lower_bound, upper_bound, sort=False):
    """
    pre: size >= 0
    post: returns a list of randomly chosen unique integers within [lb, ub)
    :param size : positive integer
    :param lower_bound: positive integer
    :param upper_bound: positive integer
    :param sort : boolean specifying whether tab should be sorted
    """
    try:
        tab = random.sample(range(lower_bound, upper_bound), size)
        if sort:
            tab = sorted(tab)
        return tab
    except ValueError:
        print('Sample size exceeded population size.')

def bench_search(sizes=(100, 200, 500, 1000, 2000), num_inst=100, num_keys=100):
    paire_max_time = {s: [] for s in sizes}
    paire_max_efficace_time = {s: [] for s in sizes}
    random.seed(42)  # Fix random seed for reproducible results
    # Loop over instance sizes
    for s in sizes:
        # Loop over various instances of the same size
        for i in range(num_inst):
            # Generate an instance of size s including unique integers in [0, 2s)
            tab = generate_instance(s, 0, 2*s, sort=True)
            # Search for various keys in tab
            for _ in range(num_keys):

                # Temps - paire_max
                start = time.process_time()
                pos1 = paire_max(tab)
                stop = time.process_time()
                paire_max_time[s].append((stop-start) * 1000)

                # Temps - paire_max_efficace
                start = time.process_time()
                pos2 = paire_max_efficace(tab)
                stop = time.process_time()
                paire_max_efficace_time[s].append((stop-start) * 1000)

                # Check that both programs give the same result
                assert pos1 == pos2
    return paire_max_time, paire_max_efficace_time

sizes = (100, 200, 500, 1000, 2000)
pmt, pmet = bench_search(sizes)

# Compute median values, first and third quartiles
medians_pmax = [np.percentile(pmt[s], 50) for s in sizes]
medians_pmax_eff = [np.percentile(pmet[s], 50) for s in sizes]
q1_pmax = [np.percentile(pmt[s], 25) for s in sizes]
q3_pmax = [np.percentile(pmt[s], 75) for s in sizes]
q1_pmax_eff = [np.percentile(pmet[s], 25) for s in sizes]
q3_pmax_eff = [np.percentile(pmet[s], 75) for s in sizes]

# Performance de pair_max_efficace
plt.figure(figsize=(10, 6))
plt.plot(sizes, medians_pmax_eff, 'bo-', label="paire_max_efficace")
plt.plot(sizes, medians_pmax, 'ro-', label="paire_max")
plt.xticks(sizes)
plt.legend(loc=2)
plt.xlabel("Taille des instances")
plt.ylabel("Temps CPU en msec")
# add error bars
plt.errorbar(sizes, medians_pmax, yerr=[q1_pmax, q3_pmax], ecolor='r', fmt='none', capsize=5)
plt.errorbar(sizes, medians_pmax_eff, yerr=[q1_pmax_eff, q3_pmax_eff], ecolor='b', fmt='none', capsize=5)
plt.show()