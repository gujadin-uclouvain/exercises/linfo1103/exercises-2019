def paire_max_efficace(a):
    max1 = 0
    max2 = 0
    
    for g in range(len(a)):
        
        if a[g] > max1:
            if max1 > max2:
                max2 = max1
            max1 = a[g]
            
        elif a[g] > max2:
            max2 = a[g]
    
    return max1 + max2
