def paire_max(a):
    max_sum = -999999
    if len(a) >= 2:
        for g in range(len(a)):
            for h in range(g+1, len(a)):
                current_sum = a[g] + a[h]
                if current_sum > max_sum:
                    max_sum = current_sum
                    
        return max_sum
    else:
        return 1
