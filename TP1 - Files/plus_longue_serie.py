#! /usr/bin/python3

def plus_longue_serie(votes):
    """
    pre: `votes` est un tableau (list) de votes
    post: renvoie `i` tel que `votes[i]` est le premier vote
        de la plus longue suite de votes identiques
    """
    i = 0
    count = 0
    size = 0
    l_serie = [0, 0]

    while count != len(votes):
        if votes [i] == votes [count]:
            size += 1
            count += 1
            
        else:
            if size > l_serie [1] :
                l_serie = [i, size]
                
            i = count
            size = 0
            
    if size > l_serie [1] :
        l_serie = [i, size]
            
    return l_serie [0]





def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)

#Exemples tests:
if not plus_longue_serie(['A', 'A', 'B']) == 0:
    error("Plus longue serie au debut")

if not plus_longue_serie(['A', 'B', 'C', 'C']) == 2:
    error("Plus longue serie a la fin")