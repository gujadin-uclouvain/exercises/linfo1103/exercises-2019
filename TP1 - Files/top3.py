#! /usr/bin/python3

def top3(votes):
    """
    pre: `votes` est un tableau (list) de votes
    post: renvoie un tableau `t` (trié de façon décroissante) de la
        longueur des trois plus longues suites de votes identiques
    """
    i = 0
    count = 0
    size = 0
    all_size = []

    while count != len(votes):
        if votes [i] == votes [count]:
            size += 1
            count += 1
            
        else:
            all_size.append(size)
                
            i = count
            size = 0
            
    all_size.append(size)
            
    return sorted(all_size, reverse=True) [:3]





def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)

#Exemples tests:
if not top3(['A', 'B', 'C']) == [1,1,1]:
    error("Plusieurs séries de taille identique")

if not top3(['A', 'A', 'A', 'B', 'B', 'C']) == [3,2,1]:
    error("Plus longues séries ordonnées")