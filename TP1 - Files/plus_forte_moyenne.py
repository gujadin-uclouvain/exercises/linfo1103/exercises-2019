#! /usr/bin/python3
from exact_allocation import exact_allocation

def plus_forte_moyenne(n_sieges, resultat_vote):
    """
    Calcul la repartition des sieges selon la methode de la plus
    forte moyenne (variante de D Hondt)
    pre: `n_sieges` > 0
    pre: len(`resultat_vote`) > 1
    pre: `resultat_vote[i]` >= 0
        pour tout `i` tel que 0 <= `i` < len(`resultat_vote`)
    post: retourne un tableau `t` de meme longueur que `resultat_vote`
        et tel que `t[i]` est le nombre de sieges attribues au parti
        correspondant a `resultat_vote[i]`
    """
    return -1



def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)

#Exemples de test:
if not plus_forte_moyenne(11, [437, 478, 85]) == [5,5,1]:
    error("Exemple de la méthode de D'Hondt")
