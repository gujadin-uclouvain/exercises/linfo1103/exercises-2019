#! /usr/bin/python3

from exact_allocation import exact_allocation

def plus_fort_reste(n_sieges, resultat_vote):
    """
    Calcul la repartition des sieges selon la methode du plus
    fort reste (variante du quotient de Hare)
    pre: `n_sieges` > 0
    pre: len(`resultat_vote`) > 1
    pre: `resultat_vote[i]` >= 0
        pour tout `i` tel que 0 <= `i` < len(`resultat_vote`)
    post: retourne un tableau `t` de même longueur que `resultat_vote`
        et tel que `t[i]` est le nombre de sieges attribues au parti
        correspondant a `resultat_vote[i]`
    """
    l = exact_allocation(n_sieges, resultat_vote)
    sieges = [0]*len(l)
    counter = 0
    while counter != "exit":
        if counter < len(l):
            temp = round(l[counter] % 1 , 3)
            sieges[counter] = int(l[counter] - temp)
            l[counter] = temp
            counter += 1
        
        else:
            counter = "exit"
    
    
    
    return l


"""
def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)

#Exemples des test:
if not plus_fort_reste(5, [437, 478, 85]) == [2,2,1]:
    error("Exemple du quotient de Hare")
"""
print(plus_fort_reste(5, [437, 478, 85]))