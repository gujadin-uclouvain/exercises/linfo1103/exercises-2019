#! /usr/bin/python3

from exact_allocation import exact_allocation

def plus_fort_reste(n_sieges, resultat_vote):
    """
    Calcul la repartition des sieges selon la methode du plus
    fort reste (variante du quotient de Hare)
    pre: `n_sieges` > 0
    pre: len(`resultat_vote`) > 1
    pre: `resultat_vote[i]` >= 0
        pour tout `i` tel que 0 <= `i` < len(`resultat_vote`)
    post: retourne un tableau `t` de même longueur que `resultat_vote`
        et tel que `t[i]` est le nombre de sieges attribues au parti
        correspondant a `resultat_vote[i]`
    """
    l_sieges = []
    l = exact_allocation(n_sieges, resultat_vote)

    for i in range (len(l)):
        l_sieges.append( int( round(l[i], 0) ) )
        l[i] = round(l[i] % 1 , 3)
        
    n_sieges -= sum(l_sieges)

    while n_sieges != 0:
        ind = l.index( max(l) )
        l_sieges[ind] += 1
        n_sieges -= 1
        l[ind] = 0
        
    return l_sieges
        
print(plus_fort_reste(5, [437, 478, 85]))
