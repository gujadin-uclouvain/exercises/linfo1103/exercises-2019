#! /usr/bin/python3

def exact_allocation(n_sieges, resultat_vote):
    """
    Calcule la repartition exacte des sièges
    pre: `n_sieges` représente le nombre de sièges disponibles
        et `resultat_vote` est un tableau du nombre de voies tel
        que `resultat_vote`[`i`] est le nombre de votes pour
        le parti à l indice `i`
    post: renvoie un tableau `t` tel que `t`[`i`] correspond
        à l allocation exacte du parti à l indice `i`
    """
    exact_list = []
    tot_vote = 0
    for vote in resultat_vote:
        tot_vote += vote
        
    for vote in resultat_vote:
        x = vote/(tot_vote/n_sieges)
        exact_list.append(round(x, 3))

    return exact_list


def error(txt):
    print("Erreur !")
    print(txt)
    exit(1)

#Exemples tests:
if not exact_allocation(5, [437, 478, 85]) == [2.185, 2.390, 0.425]:
    error("Example du quotient de Hare")

if not exact_allocation(11, [437, 478, 85]) == [4.807, 5.258, 0.935]:
    error("Example de la méthode de D'Hondt")
